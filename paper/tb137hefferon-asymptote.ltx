% $Date: 2022/12/30 17:24:03 $
% This template file is public domain.
%
% TUGboat class documentation is at:
%   http://mirrors.ctan.org/macros/latex/contrib/tugboat/ltubguid.pdf
% or
%   texdoc tugboat

\documentclass[final]{ltugboat}

\usepackage{microtype}
\usepackage{graphicx}
\usepackage[hidelinks,pdfa]{hyperref}

% JH
\usepackage{xcolor}
\usepackage{listings}
\definecolor{JHgray}{gray}{0.9}
\lstset{basicstyle=\lst@ifdisplaystyle\small\fi\ttfamily,
        %backgroundcolor=\color{JHgray},
        columns=fullflexible, %
        keepspaces=true,commentstyle=\slshape,
        language=C++
}

\newcommand{\Asymptote}{Asymptote}
\newcommand{\GL}{\acro{GL}}
\newcommand{\PRC}{\acro{PRC}}
\newcommand{\mpkern}{\kern-.04em }
\def\3D{\acro{3D}}

%%% Start of metadata %%%

\title{Using \Asymptote{} like \MP}

% repeat info for each author; comment out items that don't apply.
\author{Jim Hef{}feron}
\address{Mathematics and Statistics \\University of Vermont}
\netaddress{jim.hefferon (at) gmail dot com}
\personalURL{https://hefferon.net}
%\ORCID{0}
% To receive a physical copy of the TUGboat issue, please include the
% mailing address we should use, as a comment if you prefer it not be printed.

%%% End of metadata %%%

\begin{document}
\maketitle

\begin{abstract}
\Asymptote{} is a vector graphics language for technical
drawing that fits very well with \TeX, \LaTeX{}, and friends. 
It deserves to be more widely known.

One appealing thing is
that it is in part based on algorithms from \MF{} and \MP{} but
it extends those to three dimensions. 
I'll discuss a couple of workflow
issues that a beginner to \Asymptote{} who is coming from \MP{} might
find useful, in particular using a single source file to output many
related graphics.
\end{abstract}

\section{Introduction}
% Many readers may be familiar with \MP\mpkern.
I wrote a book years ago using \MP{} and found that it had many advantages.  
I cannot draw, at all, and it was a comfort
to be able to tell the 
computer to, say, make this line to be exactly two thirds the length
of that other line.

But the best feature is that \MP{} fits a person who thinks mathematically.
For example there is a simple way to find where two lines intersect.

However, having worked with \MP{} a lot, I was aware of some warts.
For me the two biggest are lack of any real \3D\ abilities,
and that programming in the language can be \ldots\ quirky.
So when I saw
the new \Asymptote{} system I was eager to try it.
It has been very good.



\section{Overview}
\Asymptote{} is
a powerful descriptive vector graphics language.
It provides a natural coordinate-based framework for technical drawing.
\LaTeX{} typesets the text and equations.\tburlfootnote{https://asymptote.sourceforge.io/asymptote.pdf}

\begin{itemize}
\item
  It is inspired by \MP\mpkern, including
  generalizing \MP's path construction algorithms 
  to three dimensions.\tburlfootnote{https://asymptote.sourceforge.io/gallery/3Dgraphs/}
  It inherits \MP's impedance match with a mathematical mindset.
  But it is a more standard programming language, including
  declared types, familiar syntax, and  
  \IEEE\ floating point numbers.
\item
  It outputs high-quality PostScript, Open\GL, \PDF, \SVG, Web\GL, and \acro{V3D},
  as well as
  \3D vector Web\GL\ graphics
  for \HTML\ files
  and \3D vector \PRC\ graphics for \PDF\ files.
\item
  It uses deferred drawing to solve size constraint issues between
  fixed-sized objects, such as labels and arrowheads, 
  and objects that should scale with figure size.
\item
  It runs on Unix, \macOS, and Windows, and is under continuing development.
\end{itemize}

You can even try it in your browser without installing it, 
using the Asymptote Web
Application.\tburlfootnote{http://asymptote.ualberta.ca/}



\section{Compared to \TikZ}
Many readers will be familiar with \TikZ{} so I will briefly compare with that
system.

I have not used \TikZ{} much, and have not used it lately at all.
But I lined up the two when I was starting my latest project, some years ago.
They have many of the same strengths.
I found that \Asymptote{} had
some technical advantages, including the native \3D graphics,
and I also found \TikZ{} harder to program in.

Another difference, relevant here, is that the basic paradigm in \TikZ{}
is that your figures are in your document, generated when the document is 
generated.
In \Asymptote{} the paradigm is that they are generated outside the document.
(Yes, you can generate stand-alone in \TikZ{} and yes, 
you can include \Asymptote{} code in a document.)

I had a bad experience in the past
when I was using \PSTricks{} and the \LaTeX{} world switched
to \pdflatex. 
That left me with a preference for a dependence chain
that is shorter and broader 
over one that is taller and thinner.
I saw this again recently when there was an issue with \Ghostscript{}
and I was able to put the external \PDF{} figures into my repository
until the issue was resolved.
So one factor in my comparison tipping me to \Asymptote{} was a preference for
generating figures independently from the documents.



\section{Working in \MP{}}
The basic structure of \MP{} input is that one file holds many figures.
This will output a graphic into a file numbered 1 and another
graphic into a file numbered~2.
\begin{lstlisting}[language=TeX]
beginfig (1)  % MetaPost
  % first figure drawing commands
endfig;
beginfig (2)
  % second figure drawing commands
endfig;
\end{lstlisting}
Putting a number of related sources in the same file is convenient. 
For instance, if these are drawings for a calculus lecture then 
you might at the top of the file declare
\lstinline{VECTOR_THICKNESS=0.8pt},
and use that in lots of the figures.

I will describe two adjustments that may help a person coming
from \MP{} and that took me some time to dope out. 



\section{Adjustment one: multiple figures per file}
Here is the skeleton to put multiple figures in one \Asymptote{} file.

This outputs a file \lstinline{test000.pdf} containing the graphic
with a diagonal red line.
\begin{lstlisting}[language=C++]
string OUTPUT_FN = "test%03d";
// =============
picture pic; int picnum = 0;
unitsize(pic, 1cm);  // dist from x=0 to x=1
draw(pic, (0,0)--(1,1), red); // make a line 
shipout(format(OUTPUT_FN,picnum),
         pic, format="pdf");  

// =============
picture pic; int picnum = 1;
  ...
shipout(...);
\end{lstlisting}
Some things to notice in that code:

\begin{itemize}
\item
The \lstinline{OUTPUT_FN} gives all file names the same structure.
So if you have lots of graphics (my current book has more than 2000) then
it is easier to work with them.
Of course, \lstinline{%03d} 
gives the picture number 
as a three decimal place integer.
I find two decimal places is too tight.

\item
A second picture begins with the declarations
\lstinline{picture pic}
and \lstinline{int picnum = 1} and ends with
an identical \lstinline{shipout(...)}, as shown.
  
\item I write \lstinline{picnum = 0} and \lstinline{picnum = 1}, etc.,
instead of \lstinline{picnum = picnum+1}.
When you go back a week later into a file with eighty pictures,
looking to fix a bug in
the fifty-third, you want it this way.

\item
Because of the multiple outputs from a single input,
lots of commands need a picture argument.
The code has it in the \lstinline{draw(...)} command.
If you leave out the \lstinline{pic} then the line gets drawn somewhere
(that is, there is no error) but not in the output
file where you are looking for it.
The \lstinline{pic} is also in the \lstinline{unitsize(...)} command.
\end{itemize}

A comment:~\MP{} is set up so the figure code blocks are isolated, in that
if, say, you set \lstinline{x=1} inside one
\lstinline{beginfig ... endfig} then that
does not affect an \lstinline{x} inside another.
There is no such isolation here.
But I sometimes reuse variables in a number of related figures so I'm
happy with
this.


\section{Adjustment two: style files}
This is related to the prior adjustment
in that
one way that having multiple outputs from a single input is helpful is 
to enforce uniformity.
You can have parameters such as line thickness or font size,
and specify them in just the one file.

But the same applies across multiple files.
It is useful to have every \Asymptote{} source file
get desired global parameters from a single file.

You bring in a file with the \lstinline{import fn} command.
For instance, \Asymptote{} has a standard file called \lstinline{settings.asy}
that you usually want to bring in.
\begin{lstlisting}
import settings; 
settings.outformat="pdf";
\end{lstlisting}
If that file is in the list of directories searched by the \Asymptote{}
system then you are good.
The list is what you might guess:~first the current directory,
then one or more directories given by the environment variable 
\lstinline{ASYMPTOTE_DIR}, etc.
% (3)~the directory given by 
% \lstinline{ASYMPTOTE_HOME} (or else \lstinline{.asy} in the 
% user's home directory); 
% (4)~the \Asymptote{} system directory;
% (5)~the \Asymptote{} examples directory, such as
% \lstinline{/usr/local/share/doc/asymptote/examples}.

Here is part of the style file for my current book.
\begin{lstlisting}
import fontsize;
defaultpen(fontsize(9.24994pt));
import texcolors;
pen darkgrey_color=rgb("595241");  
pen lightgrey_color=rgb("E0D4BE"); 
pen white_color=rgb("FFFFFF");
pen lightblue_color=rgb("ACCFCC");
pen red_color=rgb("8A0917");       
// Use these names, not prior ones
pen highlightcolor=red_color;
pen backgroundcolor=lightblue_color;
pen boldcolor=darkgrey_color;
pen lightcolor=lightgrey_color;
pen verylightcolor=white_color;
\end{lstlisting}
(I like the color names such as \lstinline{highlightcolor} 
to make graphics because I sometimes
adjust the color scheme, for instance when I get a proof copy
from the publisher.)

In my current project I also have a \LaTeX{} style file containing the
font information and document-specific macros
that is used by every \Asymptote{} file, as well
as by the \LaTeX{} driver file \lstinline{book.tex},
ensuring that the figures match the text.


\section{Ending}
\Asymptote{} does a great job drawing technical graphics.
Adding some \MP-like workflow makes it even more fun.

\advance\signaturewidth by 3pt
\makesignature
\end{document}

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End:
